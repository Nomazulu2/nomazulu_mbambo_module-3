
import React, {Component} from "react";

import { TextInput, TouchableHighlight, View, Text} from "react-native";


class Homepage extends Component {
    constructor(props) {
        super(props);
        this.state = {secret: props.user.secret|| ''}

    }
    saveSecrete = ()=> {
        this.props.setSecret(this.state.secret);
    }

    logout = () => {
        this.props.logout();

    }
    render(){
        return <View>
            <Text>Enter your secret:</Text>
            <TextInput value = {this.state.secrete}
            style = {{borderWidth: 1, borderColor:"#CCC", padding: 6, }}
            onChangeText = {(secret => {this.setState({secret})})}/>
            <TouchableHighlight style = {Styles.button} onPress = {this.saveSecrete}></TouchableHighlight>
            <Text style = {styles.buttonText}>save</Text>
            <TouchableHighlight style = {styles.button} onPress = {this.logout}>
                <Text style = {styles.button}>Logout</Text>
            </TouchableHighlight>
        </View>
    }
}

export default Connect(
    ({user}) => ({user}),
    (dispatch) => bindActionCreators(ActionCreators, dispatch)
)(Homepage);
