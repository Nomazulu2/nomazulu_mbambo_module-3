import React from "react";

import{View, TouchableHighlight, Text} from "react-native";


class login_screen extends Component {

    constructor(props) {
        super(props);
        this.state = {password: null}

    }
    onTileChanged = (password)=> {
        this.setState({password});
    }

    resetAccount = () => {
        this.props.reset();

    }
    login_screen = () => {
        this.props.attemptlogin(this.state.password);

    }

    render(){
        return <View>
            <Text style = {styles.tittle}>login</Text>
            <TileMap onTileChanged = {this.onTileChanged}>
            <TouchableHighlight style = {Styles.button} onPress = {this.login}></TouchableHighlight>
            <Text style = {styles.buttonText}>login</Text>
            <TouchableHighlight style = {styles.button} onPress = {this.resetAccount}>
                <Text style = {styles.button}>reset Account</Text>
            </TouchableHighlight>
            </TileMap>
        </View>
    }
}

export default connect(
    ({user}) => ({user}),
    (dispatch) => bindActionCreators(ActionCreators, dispatch)
)(login_screen);
